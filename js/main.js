function touchStarted() {
  getAudioContext().resume();
}

var audioUrl = "audio/cepasaDarkBeauty.mp3";

// Instantiate wavesurfer
var wavesurfer = WaveSurfer.create({
  container: "#wave",
  barWidth: 3,
  barHeight: 2, // the height of the wave
  barGap: 1,
  barMinHeight: 3,
  waveColor: "#fff",
  progressColor: "#c7c27e",
  cursorColor: "#c7c27e",
  cursorWidth: 0,
  height: 100,
  pixelRatio: 2,
  responsive: true,
  scrollParent: false,
});

wavesurfer.on("ready", function () {
  $("#loading").hide();
});

// Override the renderer
wavesurfer.drawer.drawBars = function (peaks, channelIndex, start, end) {
  return customDrawBars(wavesurfer, peaks, channelIndex, start, end);
};

// Load the mp3
wavesurfer.load(audioUrl);

// Custom drawbars function
// Note: this is based on the source code from the wavesurfer github
// project, but with a few changes to the code that calls
// wavesurfer.drawer.fillRect
function customDrawBars(wavesurfer, peaks, channelIndex, start, end) {
  return wavesurfer.drawer.prepareDraw(peaks, channelIndex, start, end, function (_ref) {
    var absmax = _ref.absmax,
      hasMinVals = _ref.hasMinVals,
      height = _ref.height,
      offsetY = _ref.offsetY,
      halfH = _ref.halfH,
      peaks = _ref.peaks;

    // if drawBars was called within ws.empty we don't pass a start and
    // don't want anything to happen
    if (start === undefined) {
      return;
    } // Skip every other value if there are negatives.

    var peakIndexScale = hasMinVals ? 2 : 1;
    var length = peaks.length / peakIndexScale;
    var bar = wavesurfer.params.barWidth * wavesurfer.params.pixelRatio;
    var gap =
      wavesurfer.params.barGap === null
        ? Math.max(wavesurfer.params.pixelRatio, ~~(bar / 2))
        : Math.max(wavesurfer.params.pixelRatio, wavesurfer.params.barGap * wavesurfer.params.pixelRatio);
    var step = bar + gap;
    var scale = length / wavesurfer.drawer.width;
    var first = start;
    var last = end;
    var i;

    var topRatio = 0.7;
    var bottomRatio = 0.25;
    var topBottomGap = 1;

    for (i = first; i < last; i += step) {
      var peak = peaks[Math.floor(i * scale * peakIndexScale)] || 0;
      var h = Math.abs(Math.round((peak / absmax) * height));

      // Upper bars
      var fx = i + wavesurfer.drawer.halfPixel;
      var fy = height * topRatio + offsetY - h * topRatio;
      var fwidth = bar + wavesurfer.drawer.halfPixel;
      var fheight = h * topRatio;

      wavesurfer.drawer.fillRect(fx, fy, fwidth, fheight);

      // Recalculate for lower bar
      fy = height * topRatio + offsetY + topBottomGap;
      fheight = h * bottomRatio;

      wavesurfer.drawer.fillRect(fx, fy, fwidth, fheight);
    }
  });
}
